// console.log("Hello World");

// Name is a parameter. Used to store info
function printName(name){
	console.log("My Name is "+ name);
}

// Data passed into function invocation can be received by the function
// This is what we call an argument
printName("Daphne");//Daphne is an argument
console.log(name);

function checkDivisibilityBy8(num){

	let remainder = num % 8;
	console.log("The remainder of "+ num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is "+ num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(16);

// Variables can also be passed as argument
let sampleVariable = 64;
checkDivisibilityBy8(sampleVariable);

/*
	Mini-Activity
		1. Create a function which is able to receive data as an argument
			- This function should be able to receive the same of your favorite superhero
			- Display name of your favorite superhero in console

*/

function printFaveSuperhero(hero){

	console.log("My favorite superhero is: "+ hero);
}

printFaveSuperhero("Iron Man");


function otherPrintName(name){
	console.log("My name is "+ name);
}

otherPrintName();
// Function calling without arguments will result to undefined parameters

otherPrintName(["Mark", " Jayson", " Raf"]);

// Using Multiple Parameters
// Multiple "arguments" will correspond to number of "parameters" declared in a function in succeeding order

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}
createFullName("Juan", "Dela", "Cruz");
// "Juan" will be stored in parameter first name
// "Dela" in middle name
// "Cruz" in last name

createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "Jr.");

// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

/*
	Mini-Activity:
		Create a function which will be able to receive 5 arguments 
		- Receive 5 of your favorite songs
		- Display/print the passed 5 favorite songs in the console when function is invoked
*/

function printFiveFaveSongs(song1, song2, song3, song4, song5){
	console.log("Your top 5 songs are: "+ song1 +", "+ song2 + ", " + song3 + ", " + song4 + ", and " + song5);
}
printFiveFaveSongs("Self Control","Push","Serotonia","Slow Dancing in the Dark","Leave Out All the Rest");

// Return Statement 
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
// Return is used so we can manipulate it without it printing 
// Storing, not printing

function returnFullName(firstName, middleName, lastName){

	return firstName + " " + middleName + " " + lastName
	console.log("This message will not be printed");//because after return, nothing else follows

}

let completeName = returnFullName("Jane", "Shatur", "Regina");
console.log(completeName);


function returnAddress (city, country) {
	let fullAddress = city + ", " + country;
	return fullAddress;

}

let myAddress = returnAddress("Manila", "Philippines");
console.log(myAddress);


// This only prints
// function printPlayerInfo(username, level, job){
// 	console.log("Username: "+ username);
// 	console.log("Level: "+ level);
// 	console.log("Job: " + job);
// }

// let user1 = printPlayerInfo("ikawLangSapatNa", 99, "Mage");
// console.log(user1);//undefined b/c there is no return statement

function printPlayerInfo(username, level, job){
	return "username: " + username + " level: " + level + " job: " + job;
}

let user1 = printPlayerInfo("ikawLangSapatNa", 99, "Mage");
console.log(user1);
